﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using WebApplication1.Models;
using WebApplication1.Repositories;

namespace WebApplication1.Controllers
{
    public class StudentsController : Controller
    {
        private readonly IBaseRepository<Student> _repository;

        public StudentsController(IBaseRepository<Student> repository)
        {
            _repository = repository;
        }

        public IActionResult Index()
        {
            IList<Student> students = _repository.FindList();
            return View(students);
        }
    }
}
