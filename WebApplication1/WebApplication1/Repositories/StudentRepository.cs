﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace WebApplication1.Repositories
{
    public class StudentRepository : IBaseRepository<Student>
    {
        private readonly UniversityContext _ctx;

        public StudentRepository(UniversityContext ctx)
        {
            _ctx = ctx;
        }

        public IList<Student> FindList()
        {
            return _ctx.Students
                .OrderBy(e => e.FirstName)
                .ThenBy(e => e.LastName)
                .ToList();
        }
    }
}
